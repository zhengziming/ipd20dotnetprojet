﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for AdministratorDialog.xaml
    /// </summary>
    public partial class AdministratorDialog : Window
    {
        public AdministratorDialog()
        {
            InitializeComponent();
            lblLoginName.Content = Globals.loginName;
            //
          
        }

        private void btLogout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.Show();
            Close();
        }

        private void btAddVeterinary_Click(object sender, RoutedEventArgs e)
        {
            AddVeterinaryPage page = new AddVeterinaryPage();
            AdminFrame.NavigationService.Navigate(page);
        }

        private void btAddClient_Click(object sender, RoutedEventArgs e)
        {
            AddClientPage page = new AddClientPage();
            AdminFrame.NavigationService.Navigate(page);
        }

        private void btAddPet_Click(object sender, RoutedEventArgs e)
        {
            AddPetPage page = new AddPetPage();
            AdminFrame.NavigationService.Navigate(page);
        }

        private void btAddSchedule_Click(object sender, RoutedEventArgs e)
        {
            AddSchedulePage page = new AddSchedulePage();
            AdminFrame.NavigationService.Navigate(page);
        }
        private void btCheckSchedule_Click(object sender, RoutedEventArgs e)
        {

            PageAdminScheduleList page = new PageAdminScheduleList();
            AdminFrame.NavigationService.Navigate(page);
        }
        private void btCheckClient_Click(object sender, RoutedEventArgs e)
        {
            PageCheckClient page = new PageCheckClient();
            AdminFrame.NavigationService.Navigate(page);
        }
        private void btCheckPet_Click(object sender, RoutedEventArgs e)
        {
            PageCheckPet page = new PageCheckPet();
            AdminFrame.NavigationService.Navigate(page);
        }
        private void btCheckVeterinary_Click(object sender, RoutedEventArgs e)
        {
            PageCheckVeterinary page = new PageCheckVeterinary();
            AdminFrame.NavigationService.Navigate(page);
            
           // PageEditVeterinary page1 = new PageEditVeterinary(null);
           // AdminEditFrame.NavigationService.Navigate(page1);
        }
    }
}
