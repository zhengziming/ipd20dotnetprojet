﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for PageAdminScheduleList.xaml
    /// </summary>
    /// 
    public  enum Status
    {
        Pending, Done, Cancel
    }

    public partial class PageAdminScheduleList : Page
    {
        public PageAdminScheduleList()
        {
            InitializeComponent();
            getAllData();
        }
        private void getAllData()
        {
            Globals.ctx = new test1zm2Entities();
            // lvSchedule.ItemsSource = Globals.ctx.schedules.ToList(); 

            lvSchedule.ItemsSource = (from s in Globals.ctx.ScheduleListViews select s).ToList();
        }

        private void lvSchedule_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           

        ScheduleListView v = (ScheduleListView)lvSchedule.SelectedItem;
            lblPetName.Content = v.PetName;
           

            var query = from it in Globals.ctx.schedules
                        where it.Id.Equals(v.Id) 
                        select it;
            schedule s = query.First();
            dpDate.Text = s.Date.ToString();


            comboStatus.ItemsSource = Enum.GetValues(typeof(Status));
            Status status = (Status)Enum.Parse(typeof(Status), s.Status);
            int index = Array.IndexOf(Enum.GetValues(status.GetType()), status);
          
            comboStatus.SelectedIndex =index;

        }

        
        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            ScheduleListView v = (ScheduleListView)lvSchedule.SelectedItem;
          
            var query = from it in Globals.ctx.schedules
                        where it.Id.Equals(v.Id)
                        select it;
            schedule s = query.First();
          
            try

            {
                
                s.Date = dpDate.SelectedDate.Value;
                s.Status = comboStatus.SelectedItem.ToString();



                //  Globals.ctx.veterinaries.Add(v);

                Globals.ctx.SaveChanges();

                MessageBox.Show("Record Update successfully.");



            }

            catch (Exception ex)

            {

                MessageBox.Show(ex.InnerException.ToString());

            }

            getAllData();
            lblPetName.Content = null;
            dpDate.Text = null;
            
        }
    }
}
