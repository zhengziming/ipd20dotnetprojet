﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for PageCheckPet.xaml
    /// </summary>
    public partial class PageCheckPet : Page
    {
        public PageCheckPet()
        {
            InitializeComponent();
            getAllData();
/*
            var query = from it in Globals.ctx.clients
                        orderby it.LastName
                        select it;

            foreach (client c in query)
            {
                string str = c.Id + "  " + c.LastName + ' ' + c.FirstName;
                comboClientId.Items.Add(str);

            }
*/
           
        }
        private void getAllData()
        {
            Globals.ctx = new test1zm2Entities();
            // lvSchedule.ItemsSource = Globals.ctx.schedules.ToList(); 

            lvPet.ItemsSource = (from s in Globals.ctx.pets select s).ToList();
        }

        private void lvPet_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            pet v = (pet)lvPet.SelectedItem;



            tbPetName.Text = v.PetName;

       

            var image = new BitmapImage();
            using (var mem = new MemoryStream(v.Photo))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            btImage.Source = image;



        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            pet v = (pet)lvPet.SelectedItem;
            try

            {

                v.PetName = tbPetName.Text;

                //
                byte[] data = bitmapImageToByteArray((BitmapImage)btImage.Source);
                v.Photo = data;


                //  Globals.ctx.veterinaries.Add(v);

                Globals.ctx.SaveChanges();

                MessageBox.Show("Record Update successfully.");



            }

            catch (Exception ex)

            {

                MessageBox.Show(ex.InnerException.ToString());

            }
        }

        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "images (*.png)|*.png|(*.jpg)|*.jpg|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    // imageInBytes = File.ReadAllBytes(openFileDialog.FileName);
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(openFileDialog.FileName);
                    bitmap.EndInit();
                    btImage.Source = bitmap;
                }
                catch (IOException)
                {
                    MessageBox.Show("Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException)
                {
                    MessageBox.Show("Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "images (*.png)|*.png|(*.jpg)|*.jpg|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    //imageInBytes = File.ReadAllBytes(openFileDialog.FileName);
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(openFileDialog.FileName);
                    bitmap.EndInit();
                    btImage.Source = bitmap;
                }
                catch (IOException )
                {
                    MessageBox.Show( "Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }
    }
}