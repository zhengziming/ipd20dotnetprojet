﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for AddVeterinaryPage.xaml
    /// </summary>
    public partial class AddVeterinaryPage : Page
    {
        public AddVeterinaryPage()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            // test1zm2Entities ctx = new test1zm2Entities();
            string un = tbUsername.Text;
          

            var query = from it in Globals.ctx.veterinaries
                        where it.Username.Equals(un)
                        select it;
            int i = query.Count();
            if (i > 0)
            {
                MessageBox.Show("Username used!");
            }
            else
            {
               
           

                try

                {

                    veterinary v = new veterinary();

                
                        v.FirstName = tbFirstName.Text;
                        v.LastName = tbLastName.Text;
                    v.Phone = tbPhone.Text;
                    v.Address = tbAddress.Text;
                    v.Username = tbUsername.Text;
                    v.Password = tbPassword.Text;
                

                   Globals.ctx.veterinaries.Add(v);

                    Globals.ctx.SaveChanges();

                    MessageBox.Show("Record Inserted successfully.");
                    this.Visibility = Visibility.Hidden;


                }

                catch (Exception ex)

                {

                    MessageBox.Show(ex.InnerException.ToString());

                }

               
            }

            //

           
        }
    }
}
