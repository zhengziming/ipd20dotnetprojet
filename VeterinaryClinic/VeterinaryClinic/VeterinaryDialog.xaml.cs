﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for VeterinaryDialog.xaml
    /// </summary>
    public partial class VeterinaryDialog : Window
    {
        public VeterinaryDialog()
        {
            InitializeComponent();

            lblLoginName.Content = Globals.loginName;

            PageVeterinaryScheduleList page = new PageVeterinaryScheduleList();
            VeterinaryFrame.NavigationService.Navigate(page);
        }
        private void btLogout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.Show();
            Close();
        }
    }
}
