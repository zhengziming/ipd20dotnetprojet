﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //public static test1zm2Entities ctx = new test1zm2Entities();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btLogin_Click(object sender, RoutedEventArgs e)
        {
           // test1zm2Entities ctx = new test1zm2Entities();

            if (comboLoginAs.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose an option.");
            }            
            else if(comboLoginAs.Text.Equals("Administrator"))
            {
                string un = tbUsername.Text;
                string pw = tbPassword.Password;

                var query = from it in Globals.ctx.admins
                            where it.username.Equals(un) && it.password.Equals(pw) 
                            select it;
                int i = query.Count();         
                if(i > 0)
                {
                    foreach (admin a in query)
                    {                      
                        if (a.username == un && a.password == pw)
                        {
                            Globals.loginName = a.username;
                            Globals.loginId = -1;
                            AdministratorDialog dialog = new AdministratorDialog();
                            dialog.Show();
                            Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Username or Password is wrong!");
                }              
            }
            else if (comboLoginAs.Text.Equals("Veterinary"))
            {
                string un = tbUsername.Text;
                string pw = tbPassword.Password;

                var query = from it in Globals.ctx.veterinaries
                            where it.Username.Equals(un) && it.Password.Equals(pw)
                            select it;
                int i = query.Count();
                if (i > 0)
                {
                    foreach (veterinary v in query)
                    {
                        if (v.Username == un && v.Password == pw)
                        {
                            Globals.loginName = v.FirstName + ' ' + v.LastName;
                            Globals.loginId = v.Id;
                            VeterinaryDialog dialog = new VeterinaryDialog();
                            dialog.Show();
                            Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Username or Password is wrong!");
                }
            }
            tbPassword.Password = "";
            tbUsername.Text = "";
        }

        private void btExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
