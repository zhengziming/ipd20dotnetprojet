﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for AddClientPage.xaml
    /// </summary>
    public partial class AddClientPage : Page
    {
        public AddClientPage()
        {
            InitializeComponent();
        }
        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
          //  test1zm2Entities ctx = new test1zm2Entities();

            try
            {
                  client c = new client();


                c.FirstName = tbFirstName.Text;
                c.LastName = tbLastName.Text;
                c.Phone = tbPhone.Text;
                c.Email = tbEmail.Text;

                Globals.ctx.clients.Add(c);
                Globals.ctx.SaveChanges();
                MessageBox.Show("Record Inserted successfully.");           
            }
            catch (Exception ex)
            {
               MessageBox.Show(ex.InnerException.ToString());
            }
            this.Visibility = Visibility.Hidden;
        }
    }
}
