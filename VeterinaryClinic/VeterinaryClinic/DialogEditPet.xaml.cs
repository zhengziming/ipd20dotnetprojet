﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for DialogEditPet.xaml
    /// </summary>
    public partial class DialogEditPet : Window
    {
        public DialogEditPet()
        {
            InitializeComponent();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                pet p = new pet();

                string str = comboClientId.SelectedItem.ToString();
                string[] sArray = str.Split(' ');
                int clientId;
                int.TryParse(sArray[0], out clientId);

                p.clientId = clientId;
                p.PetName = tbPetName.Text;

                byte[] data = bitmapImageToByteArray((BitmapImage)btImage.Source);
                p.Photo = data;
                Globals.ctx.pets.Add(p);
                Globals.ctx.SaveChanges();
                MessageBox.Show("Record Inserted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }
            this.Visibility = Visibility.Hidden;
        }



      
        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "images (*.png)|*.png|(*.jpg)|*.jpg|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    // imageInBytes = File.ReadAllBytes(openFileDialog.FileName);
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(openFileDialog.FileName);
                    bitmap.EndInit();
                    btImage.Source = bitmap;
                }
                catch (IOException )
                {
                    MessageBox.Show("Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException )
                {
                    MessageBox.Show("Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }
        
    }
}
