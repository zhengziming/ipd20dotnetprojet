﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for AddSchedulePage.xaml
    /// </summary>
    public partial class AddSchedulePage : Page
    {
       // test1zm2Entities ctx = new test1zm2Entities();
        public AddSchedulePage()
        {
            InitializeComponent();

            getPetId();
            getVeterinaryId();

        }
        private void getPetId()
        {
            string str;

            var query = from it in Globals.ctx.pets.Include("client")
                        orderby it.PetName, it.client.LastName
                        select it;

            foreach (pet p in query)
            {
                str = p.Id + "  " + p.PetName + " -> " + p.client.LastName + ' ' + p.client.FirstName;
                comboPetId.Items.Add(str);
            }
        }

        private void getVeterinaryId()
        {

            string str;
            var query = from it in Globals.ctx.veterinaries
                   orderby it.LastName
                   select it;

           foreach (veterinary v in query)
           {
                str = v.Id + "  " + v.LastName + ' ' + v.FirstName;
               comboVeterinaryId.Items.Add(str);

           }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {

            
            try
            {
                schedule s = new schedule();
               

                string str = comboPetId.SelectedItem.ToString();
                string[] sArray = str.Split(' ');
                int petId;
                int.TryParse(sArray[0], out petId);
                s.PetId = petId;

                str = comboVeterinaryId.SelectedItem.ToString();
                sArray = str.Split(' ');
                int veterinaryId;
                int.TryParse(sArray[0], out veterinaryId);
                s.VeterinaryId = veterinaryId;

                DateTime date = dpDate.SelectedDate.Value;
                s.Date = date;
                s.Status = "Pending";

                Globals.ctx.schedules.Add(s);
                Globals.ctx.SaveChanges();
                MessageBox.Show("Record Inserted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }
            this.Visibility = Visibility.Hidden;
            
        }
    }
}
