﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for PageCheckClient.xaml
    /// </summary>
    public partial class PageCheckClient : Page
    {
        public PageCheckClient()
        {
            InitializeComponent();
            getAllData();
        }
        private void getAllData()
        {
            Globals.ctx = new test1zm2Entities();
            // lvSchedule.ItemsSource = Globals.ctx.schedules.ToList(); 

            lvClient.ItemsSource = (from s in Globals.ctx.clients select s).ToList();
        }

        private void lvClient_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            client v = (client)lvClient.SelectedItem;




            tbFirstName.Text = v.FirstName;
            tbLastName.Text = v.LastName;
            tbPhone.Text = v.Phone;
            tbEmail.Text = v.Email;


        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            client v = (client)lvClient.SelectedItem;
            try

            {

                v.FirstName = tbFirstName.Text;
                v.LastName = tbLastName.Text;
                v.Phone = tbPhone.Text;
                v.Email = tbEmail.Text;


                //  Globals.ctx.veterinaries.Add(v);

                Globals.ctx.SaveChanges();

                MessageBox.Show("Record Update successfully.");



            }

            catch (Exception ex)

            {

                MessageBox.Show(ex.InnerException.ToString());

            }
        }
    }
}

