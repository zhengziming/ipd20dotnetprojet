﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for DialogEditClient.xaml
    /// </summary>
    public partial class DialogEditClient : Window
    {
        public DialogEditClient(client v)
        {
            InitializeComponent();

            tbFirstName.Text = v.FirstName;
            tbLastName.Text = v.LastName;
            tbPhone.Text = v.Phone;
            tbEmail.Text = v.Email;

        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
