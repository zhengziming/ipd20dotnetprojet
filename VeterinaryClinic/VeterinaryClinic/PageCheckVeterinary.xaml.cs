﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for PageCheckVeterinary.xaml
    /// </summary>
    public partial class PageCheckVeterinary : Page
    {
        public PageCheckVeterinary()
        {
            InitializeComponent();
            getAllData();


        }
        private void getAllData()
        {
            Globals.ctx = new test1zm2Entities();
            // lvSchedule.ItemsSource = Globals.ctx.schedules.ToList(); 

            lvVeterinary.ItemsSource = (from s in Globals.ctx.veterinaries select s).ToList();
        }


        private void lvVeterinary_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            veterinary v = (veterinary)lvVeterinary.SelectedItem;

           

           
            tbFirstName.Text = v.FirstName;
            tbLastName.Text = v.LastName;
            tbPhone.Text = v.Phone;
            tbAddress.Text = v.Address;
            tbUsername.Text = v.Username;
            tbPassword.Text = v.Password;


        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            veterinary v = (veterinary)lvVeterinary.SelectedItem;
            try

            {

                v.FirstName = tbFirstName.Text;
                v.LastName = tbLastName.Text;
                v.Phone = tbPhone.Text;
                v.Address = tbAddress.Text;
                v.Username = tbUsername.Text;
                v.Password = tbPassword.Text;


                Globals.ctx.veterinaries.Add(v);

                Globals.ctx.SaveChanges();

                MessageBox.Show("Record Inserted successfully.");



            }

            catch (Exception ex)

            {

                MessageBox.Show(ex.InnerException.ToString());

            }
        }
    }
}
