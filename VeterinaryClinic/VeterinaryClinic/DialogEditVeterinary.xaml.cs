﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace VeterinaryClinic
{
    /// <summary>
    /// Interaction logic for DialogEditVeterinary.xaml
    /// </summary>
    public partial class DialogEditVeterinary : Window
    {


        public DialogEditVeterinary(veterinary v)
        {
            InitializeComponent();

            tbFirstName.Text = v.FirstName;
            tbLastName.Text = v.LastName;
            tbPhone.Text = v.Phone;
            tbAddress.Text = v.Address;
            tbUsername.Text = v.Username;
            tbPassword.Text = v.Password;

        }



        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
